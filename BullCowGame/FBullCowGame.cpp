#include "FBullCowGame.h"
#include <map>
#define TMap std::map

using FString = std::string;
using int32 = int;

FBullCowGame::FBullCowGame() // default constructor
{
	Reset();
}


int32 FBullCowGame::GetCurrentTry() const { return MyCurrentTry; }
int32 FBullCowGame::GetHiddenWordLength() const { return MyHiddenWord.length(); }
bool FBullCowGame::IsGameWon() const { return bGameIsWon; }

int32 FBullCowGame::GetMaxTries() const 
{
	TMap<int32, int32> WordLenghtToMaxTries{ {3,4}, {4,7}, {5,10}, {6,16}, {7,20} };
	return WordLenghtToMaxTries[MyHiddenWord.length()];
}



void FBullCowGame::Reset()
{
	const FString HIDDEN_WORD = "planet";
	MyHiddenWord = HIDDEN_WORD;
	
	MyCurrentTry = 1;

	bGameIsWon = false;

	return;
}




EGuessStatus FBullCowGame::CheckGuessValidity(FString Guess) const
{
	if (!IsIsogram(Guess)) // if guess isn't an isogram
	{
		return EGuessStatus::Not_Isogram; // TODO write function
	}
	else if (!IsLowercase(Guess)) // if the guess isn't all lowercase
	{
		return EGuessStatus::Not_Lowercase; // TODO write function
	}
	else if (Guess.length() != GetHiddenWordLength()) // if the guess length is wrong
	{
		return EGuessStatus::Wrong_Length;
	}
	else
	{
		return EGuessStatus::OK;
	}
		
}


//receives a VALID guess, increments turn, and returns count.
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	// increment the turn number
	MyCurrentTry++;

	// setup a return variable
	FBullCowCount BullCowCount;

	// loop through all letters in the guess
	int32 HiddenWordLength = MyHiddenWord.length();
	int32 GuessLength = Guess.length();
	for (int32 GChar=0; GChar < GuessLength; GChar++)
	{
		// compare letters against hidden word
		for (int32 MHWChar = 0; MHWChar < HiddenWordLength; MHWChar++)
		{			
			// if they match then
			if (Guess[GChar] == MyHiddenWord[MHWChar]) 
			{
				if (GChar == MHWChar) // if they're in the same place
				{ 
					BullCowCount.Bulls++; // increment bulls
				}	
				else 
				{ 
					BullCowCount.Cows++; // increment cows
				}
					
			}
		}
	}
	if (Guess == MyHiddenWord) { bGameIsWon = true; }
	else { bGameIsWon = false; }
	return BullCowCount;
}

bool FBullCowGame::IsIsogram(FString Guess) const
{
	// treat 0 and 1 letter words as isograms
	if (Guess.length() <= 1) { return true; }

	TMap<char, bool> LetterSeen; // TODO setup our map
	for (auto Letter : Guess)
	{
		Letter = tolower(Letter); // handle mixed case
		if (LetterSeen[Letter]) // if the letter is in the map
		{
			return false; // we do NOT have an isogram at this stage
		} else { 
			LetterSeen[Letter] = true;	// add the letter to the map as seen
		}
			
				
			
	}
	
	
	return true; // for example in cases where /0 is entered
}

bool FBullCowGame::IsLowercase(FString Guess) const
{
	if (Guess.length() <= 1) { return true; }

	for (auto Letter : Guess)
	{
		if (!islower(Letter)) {
			return false;
		} else { return true; }
	}
	return true;
}
